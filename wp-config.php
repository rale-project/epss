<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "epss-db" );

/** MySQL database username */
define( 'DB_USER', "root" );

/** MySQL database password */
define( 'DB_PASSWORD', "" );

/** MySQL hostname */
define( 'DB_HOST', "localhost" );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define( 'AUTH_KEY',         '={s]Fun}V<?,l +*?VQ7}=}IRJF>(7*YR5w#Dad6v4^Rn=g)+mPRmo^_ol?*>4-*' );
define( 'SECURE_AUTH_KEY',  'qt?2CEoJ.tiN>nkvekiP]7erThj  <<@wMIn(@s/M9)uGA;^lF*a_;E[PRn2AG}v' );
define( 'LOGGED_IN_KEY',    'w iE$. <>eU+Tc4{Soz;!hp`zoHBfCgf&aE@K!H%4>-)D?J9&./rkR~cxij~K.Jn' );
define( 'NONCE_KEY',        'f%L#HDfc6{Xc9E+q<^G6Kou!xvzJ1YUJ65^>g>2/(}g1c/wL<6W?RMuqKEF_dT#P' );
define( 'AUTH_SALT',        'jY~d}w;Xa=`94P2p^.oAGW1oHvZ!h9B7W-JxbY5g^)Diz7X|0/<_`q`>f#?Bw_HJ' );
define( 'SECURE_AUTH_SALT', 'W+KaQ,6)iS] JV2TG^NBG^{eok[E:I!lsExl;_&fVXm(R>g%d|hc$:G_q9i:.r,s' );
define( 'LOGGED_IN_SALT',   'i#*Pf+Tf-&5e6[&~yM[%)*)YLL+w0Wlma|#j|{5Yx1&7g(C:R%zl3dd_FFZ!jw8U' );
define( 'NONCE_SALT',       'LsiAKt[+0:]dH 9uf8[@5ifv$JtP? b0<ANukfUET]j?cEjTo=Iz!Cu[zHvnX %d' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */

$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define( 'WP_DEBUG', false );
define('FS_METHOD', 'direct' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );