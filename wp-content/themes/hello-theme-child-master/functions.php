<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */
function hello_elementor_child_enqueue_scripts() {
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor-theme-style',
		],
		'1.0.0'
	);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts' );

/**
 * REGISTER POST TYPES AND TAXONOMY
*/
add_action('init', 'register_posttypes_and_taxs');

function register_posttypes_and_taxs() {
	//Start registration of Custom Post Types
	$custom_post_types = array(
	    /**
	     * Declare Product Custom Post Type ( Use this as the template for declaration )
	     *
	     * @optional slug
	     * @optional supports
	     */
	    'product' => array(
	        'label' => 'Product',
	        'labels' => array(
	            'name' => 'Products',
	            'singular_name' => 'Product'
	        ),
	        'supports' => array('title', 'editor', 'thumbnail'),
	        'public' => false,
	        'publicly_queryable' => true,
	        'show_ui' => true,
	        'show_in_nav_menus' => false,
	        'menu_icon'             => 'dashicons-format-gallery'
	    )
	    
	);

	//default args
	$defaults = array(
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'query_var' => true,
	    'capability_type' => 'post',
	    'has_archive' => true,
	    'hierarchical' => false,
	    'menu_position' => null,
	    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
	);


	foreach ($custom_post_types as $post_type => $args) {
	    // This will merge the defaults and the passed data
	    $args = wp_parse_args($args, $defaults);
	    register_post_type($post_type, $args);
	}

	// Start of registration of Custom Taxonomies
	$a_taxonomies = array(
	    array(
	        /*
	         * Custom post type name that you want to have an category
	         * Below is the template, you may follow that format
	         */
	       'the_type'       => 'product',
	       'the_taxonomies' => 'product-category',
	       'single'     => 'Product Category',
	       'plural'     => 'Product Categories',
	       'root_slug'      => ''
	    ),
	   
	);

	foreach ($a_taxonomies as $a_taxonomy) {

	    $s_the_type = $a_taxonomy["the_type"];
	    $s_the_taxonomies = $a_taxonomy["the_taxonomies"];
	    $s_single = $a_taxonomy["single"];
	    $s_plural = $a_taxonomy["plural"];
	    $s_root_slug = $a_taxonomy["root_slug"];

	    $a_labels = array(
	        'name'                       => _x( $s_plural, 'Taxonomy General Name', 'text_domain' ),
	        'singular_name'              => _x( $s_single, 'Taxonomy Singular Name', 'text_domain' ),
	        'menu_name'                  => __( $s_plural, 'text_domain' ),
	        'all_items'                  => __( 'All '.$s_plural, 'text_domain' ),
	        'parent_item'                => __( 'Parent '.$s_single, 'text_domain' ),
	        'parent_item_colon'          => __( 'Parent '.$s_single.':', 'text_domain' ),
	        'new_item_name'              => __( 'New '.$s_single.' Name', 'text_domain' ),
	        'add_new_item'               => __( 'Add New '.$s_single, 'text_domain' ),
	        'edit_item'                  => __( 'Edit '.$s_single, 'text_domain' ),
	        'update_item'                => __( 'Update '.$s_single, 'text_domain' ),
	        'separate_items_with_commas' => __( 'Separate '.strtolower($s_plural).' with commas', 'text_domain' ),
	        'search_items'               => __( 'Search '.strtolower($s_plural), 'text_domain' ),
	        'add_or_remove_items'        => __( 'Add or remove '.strtolower($s_plural), 'text_domain' ),
	        'choose_from_most_used'      => __( 'Choose from the most used '.strtolower($s_plural), 'text_domain' ),
	    );

	    $a_rewrite = array(
	        'slug'                       => $s_root_slug,
	        'with_front'                 => true,
	        'hierarchical'               => true,
	    );

	    $a_args = array(
	        'labels'                     => $a_labels,
	        'hierarchical'               => true,
	        'public'                     => true,
	        'show_ui'                    => true,
	        'show_admin_column'          => true,
	        'show_in_nav_menus'          => true,
	        'show_tagcloud'              => true,
	        'rewrite'                    => $a_rewrite,
	    );

	    register_taxonomy( $s_the_taxonomies, $s_the_type, $a_args );

	}
}

//add_action('wp_head','float_cta');
function float_cta(){
	
	if( !is_page(157) ){
		
		echo '
			<div class="float-contact-wrapper">
				<div class="the-action__btn">
					<div class="head-float">
						<i class="far fa-envelope"></i>
					</div>
					<div class="content-float">
						<a href="#" class="float-action__btn">
							Contact Us
						</a>
					</div>
				</div>
				<div class="the-action__form">
					<h3>CONTACT EPPS</h3>
				'. do_shortcode('[contact-form-7 id="77" title="Contact EPPS Form"]') .'
				</div>
			</div>
		';
		
	}
	
}

add_action('wp_footer','script_action_cta_float');
function script_action_cta_float(){
	
	echo '<script>
			jQuery(document).ready(function(){
				jQuery(".float-action__btn").on("click",function(e){
					e.preventDefault();
					
					if( jQuery(".the-action__form").hasClass("shownow") ){
						jQuery(".the-action__form").removeClass("shownow");
					}else{
						jQuery(".the-action__form").addClass("shownow");
					}
				});
					document.addEventListener( "wpcf7mailsent", function( event ) {
					  // For the form at https://www.framefreakstudio.com/application/
					  if ( 77 === event.detail.contactFormId ) {
						location = "epss.auwebstaging.com/thanks-app/";
					  }
					}, false );

			});
		</script>';
	
}